<?php

Route::middleware('web')->group(function () {
    Route::get('/{slug?}', function ($slug = 'index') {
        try {
            return view($slug);
        } catch (InvalidArgumentException $e) {
            abort(404);
        }
    })->where('slug', '.*');
});
