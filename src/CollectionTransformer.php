<?php

namespace Pipeflow;

use League\Fractal\TransformerAbstract;

class CollectionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($item)
    {
        return [
            'content' => $item->toArray()
        ];
    }
}
