<?php

namespace Pipeflow;

use Illuminate\Database\Eloquent\Model;

/**
 * Extend this abstract Eloquent Model to create a Model for a Webflow Collection.
 *
 * @var array
 */
abstract class Collection extends Model
{
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'wfid';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

	/**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
    ];
	
	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return 'slug';
	}
	
	/**
	 * Find by slug.
	 *
	 * @param string $slug
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public static function findSlug($slug)
	{
		return static::where('slug', $slug)->first();
	}
}
