<?php

if (! function_exists('pf_csrf')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
    function pf_csrf()
    {
        return csrf_field();
    }
}

if (! function_exists('pf_error')) {
    /**
     * Get the first error for the request.
     *
     * @return string
     */
    function pf_error()
    {
        return session('errors') ? session('errors')->first() : '';
    }
}

if (! function_exists('pf_input')) {
    /**
     * Retrieve an old input item.
     *
     * @param  string   $key
     * @param  string   $default
     * @param  boolean  $boolean  when true, the return value is a boolean comparing the old with the default value
     * @return mixed
     */
    function pf_input($key, $default = null, $boolean = false)
    {
        $key = str_replace(['[', ']'], ['.', ''], $key);
        $old = old($key, request($key, $boolean ? null : $default));
        
        return $boolean ? $old === $default : $old;
    }
}

if (! function_exists('pf_method')) {
    /**
     * Generate a form field to spoof the HTTP verb used by forms.
     *
     * @param  string  $method
     * @return \Illuminate\Support\HtmlString
     */
    function pf_method($method)
    {
        return method_field($method);
    }
}

if (! function_exists('pf_path')) {
    /**
     * Get the current path info for the request.
     *
     * @return string
     */
    function pf_path()
    {
        return request()->path();
    }
}

if (! function_exists('pf_status')) {
    /**
     * Get the status for the request.
     *
     * @return string
     */
    function pf_status()
    {
        return session('status');
    }
}

if (! function_exists('pf_invalid')) {
    /**
     * Checks if an input is invalid.
     *
     * @param  string   $key
     * @param  boolean  $boolean
     * @return mixed
     */
    function pf_invalid($key)
    {
        $key = str_replace(['[', ']'], ['.', ''], $key);
        $errors = session('errors');
        
        return $errors && $errors->has($key);
    }
}
