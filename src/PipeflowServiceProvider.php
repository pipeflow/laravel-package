<?php

namespace Pipeflow;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class PipeflowServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', ViewComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
