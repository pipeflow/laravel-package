<?php

namespace Pipeflow;

use Illuminate\Support\Facades\Facade;

class PipeflowFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pipeflow';
    }

    /**
     * Register the typical pipeflow routes for an application.
     *
     * @return void
     */
    public static function routes()
    {
        include_once __DIR__.'/routes.php';
    }
}
