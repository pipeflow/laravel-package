<?php

namespace Pipeflow;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ViewComposer
{
    /**
     * The auth string.
     *
     * @var string
     */
    protected $auth = '';

    /**
     * The today array.
     *
     * @var array
     */
    protected $today = [];

    /**
     * Create a new * composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->auth = Auth::check() ? 'authenticated' : 'guest';
        $this->today = [
            'year' => date('Y'),
        ];
    }
    
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'auth' => $this->auth,
            'today' => $this->today,
        ]);
    }
}
